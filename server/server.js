const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const {readdirSync} = require('fs');

require('dotenv').config();

// import routes
// const authRoutes = require('./routes/auth');

// app
const app = express();

// db
mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology:true,
    useCreateIndex:true 
})
.then(() => console.log('DB Connected'))
.catch(err=>console.log(`DB CONNECTION ERROR ${err}`));

// middlewares

app.use(morgan('dev'));  // регистратор промежуточного программного обеспечения HTTP-запросов 
// app.use(bodyParser.json({limit:'2mb'})); //чтобы разархивировать входящие данные запроса, если он заархивирован
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    limit: "150mb",
    extended: true,
  })
);
app.use(bodyParser.json());
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));
// app.use(cors());

// routes middleware
// app.use('/api', authRoutes)
readdirSync('./routes').map((r) => app.use('/api', require('./routes/' + r)));

// port

const port = process.env.PORT || 8000;

app.listen(port, ()=>console.log(`Server is running on port ${port}`))