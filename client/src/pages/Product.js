import React, {useEffect, useState} from 'react';
import { getProduct, productStar, getRelated } from '../functions/product';
import SingleProduct from "../components/cards/SingleProduct";
import { useSelector } from 'react-redux';
import ProductCard from '../components/cards/ProductCard';

const Product = ({match}) => {
    const [product, setProduct] = useState({})
    const [star, setStar] = useState(0);
    const [related, setRelated] = useState([])
    const {slug} = match.params
    const {user} = useSelector((state)=>({...state}))

    useEffect(() => {
        loadSingleProduct()
    }, [slug])

    useEffect(() => {
        if(product.ratings && user) {
            let existingRatingObject = product.ratings.find(
                (ele)=>ele.postedBy.toString() === user._id.toString()
            );
            existingRatingObject && setStar(existingRatingObject.star) // current user's star
        }
    })
    const loadSingleProduct = () => {
        getProduct(slug).then(res => {
            setProduct(res.data)
            getRelated(res.data._id).then(res => setRelated(res.data))
        })

    }

    const onStarClick = (newRating, name) => {
        setStar(newRating)
        console.log('dfvdrgrg',name, newRating, user.token)
        productStar(name, newRating, user.token)
        .then((res) => {
            console.log("rating clicked", res.data)
            loadSingleProduct()
        })
        .catch(err=>{
            console.log(err)
        })
    }
    return (
        <div className="container-fluid">
            <div className="row pt-4">
                <SingleProduct onStarClick={onStarClick} product={product} star={star}/>
            </div>
            <div className="row p-5">
                <div className="col text-center pt-5 pb-5">
                    <hr />
                    <h4>Related products</h4>
                    <hr />
                    <div className="row pb-5">
                        {related.length ? related.map((r) => (
                        <div className="col-md-4" key={r._id}>
                            <ProductCard product={r}/> 
                        </div>)) : (<div className="text-center col">No products found</div>)}

                    </div>
                </div>
            </div>
        </div>
    )
}
export default Product;
