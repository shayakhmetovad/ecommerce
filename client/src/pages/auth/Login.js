import React, {useState, useEffect} from 'react';
import {auth, googleAuthProvider} from '../../firebase';
import {toast} from 'react-toastify';
import {Button} from 'antd';
import { MailOutlined, GoogleOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import {createOrUpdateUser} from '../../functions/auth';


const Login = ({history}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [loading, setLoading] = useState(false)

    const {user} = useSelector((state) => ({...state}))
    useEffect(()=> {
        if(history.location.state) {
            return
        }
        else {
            if(user && user.token) {
                history.push('/')
            }
        }
    }, [user, history])

    let dispatch = useDispatch();


    const roleBasedRedirect = (res) => {
        // check if intended
        let intended = history.location.state;
        if(intended) {
            history.push(intended.from)
        } else {
            if(res.data.role === 'admin') {
                history.push('/admin/dashboard')
            } else {
                history.push('/user/history')
            }
        }
        
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        setLoading(true)
        try {
            const result = await auth.signInWithEmailAndPassword(email, password);
            console.log(result)
            const {user} = result
            const idTokenResult = await user.getIdTokenResult();
            createOrUpdateUser(idTokenResult.token)
            .then(res=> {
                dispatch({
                    type: 'LOGGED_IN_USER',
                    payload: {
                        email: res.data.email,
                        token:idTokenResult.token,
                        name: res.data.name,
                        role: res.data.role,
                        _id: res.data._id
                    }
                }) 
                roleBasedRedirect(res)
            })
            .catch(
                err=>console.log("Create Or Update Res", err)
            )
            // history.push('/')
            
        } catch(error) {
            console.log(error)
            toast.error(error.message)
            setLoading(false)
        }
    }
    const googleLogin = async() => {
        auth.signInWithPopup(googleAuthProvider)
        .then(async (res) => {
            const {user} = res
            const idTokenResult = await user.getIdTokenResult();
            createOrUpdateUser(idTokenResult.token)
            .then(
                res=> {
                    dispatch({
                        type: 'LOGGED_IN_USER',
                        payload: {
                          email: res.data.email,
                          token:idTokenResult.token,
                          name: res.data.name,
                          role: res.data.role,
                          _id: res.data._id
                        }
                    })
                    roleBasedRedirect(res)
                }
            )
            .catch(
                err=>console.log("Create Or Update Res", err)
            )
        })
        .catch((err) =>  {
            console.log(err)
            toast.error(err.message)
            setLoading(false)
        })
    }
    const loginForm = () => 
        <form onSubmit={handleSubmit}>
              <input placeholder="Your email" type="email" value={email} onChange={e=>setEmail(e.target.value)} className="form-control" autoFocus/> 
              <input placeholder="Your password" type="password" value={password} onChange={e=>setPassword(e.target.value)} className="form-control mt-3" /> 
              <Button 
                type="primary" 
                block shape='round' 
                className="mb-3 mt-3" 
                size="large" 
                icon={<MailOutlined/>} 
                onClick={handleSubmit} 
                disabled={!email||password.length<6}
              >
                Login with email/password  
              </Button>         
        </form>
    return (
        <div className="container p-5">
            <div className="row"> 
                <div className="col-md-6 offset-md-3">
                    {loading ? 
                     (<h4 className="text-danger">loading...</h4>) : (<h4>Login</h4>)
                    }
                   
                    {loginForm()}
                    <Button 
                        type="danger" 
                        block shape='round' 
                        className="mb-3 mt-3" 
                        size="large" 
                        icon={<GoogleOutlined/>} 
                        onClick={googleLogin} 
                        // disabled={!email||password.length<6}
                    >
                        Login with Google  
                    </Button>  
                    <Link to="/forgot/password" className="float-right text-danger">
                        Forgot password
                    </Link>
                </div>

            </div>
        </div>
    );
}
export default Login;