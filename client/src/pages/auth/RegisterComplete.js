import React, {useState, useEffect} from 'react';
import {auth} from '../../firebase';
import {toast} from 'react-toastify';
import { useDispatch } from 'react-redux';
import {createOrUpdateUser} from '../../functions/auth';


const RegisterComplete = ({history}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    // const {user} = useSelector((state) => ({...state}))

    useEffect(() => {
        setEmail(window.localStorage.getItem('emailForRegistration'));
    }, [history])
    // props.history 
    
    let dispatch = useDispatch();

    const handleSubmit = async(e) => {
        e.preventDefault();
        // validation
        if(!email || !password) {
            toast.error("Email and password are required");
            return;
        }
        if(password.length < 6) {
            toast.error("Password must be at least 6 characters");
            return;
        }
        try {
            const result = await auth.signInWithEmailLink(email, window.location.href);
            console.log('result', result);
            if(result.user.emailVerified) {
                // remove user email from local storage
                window.localStorage.removeItem('emailForRegistration');

                // Get user id token
                let user = auth.currentUser;
                await user.updatePassword(password);
                const idTokenResult = await user.getIdTokenResult();
                console.log('user:', user)
                console.log('idTokenResult', idTokenResult)
                // redux store
                
                createOrUpdateUser(idTokenResult.token)
                .then(res=> {
                    dispatch({
                        type: 'LOGGED_IN_USER',
                        payload: {
                        email: res.data.email,
                        token:idTokenResult.token,
                        name: res.data.name,
                        role: res.data.role,
                        _id: res.data._id
                        }
                    })
                })
                .catch(
                    err=>console.log("Create Or Update Res", err)
                )
                // redirect
                history.push('/');
            }
        } catch(error) {
            console.log(error)
            toast.error(error.message)
        }
    }

    const completeRegistrationForm = () => 
        <form onSubmit={handleSubmit}>
              <input type="email" value={email} className="form-control mb-3" disabled/> 
              <input type="password" value={password} onChange={e=>setPassword(e.target.value)} className="form-control" placeholder="Enter new password" autoFocus/> 
              <button type="submit"  className="btn mt-3">Register</button>         
        </form>
    return (
        <div className="container p-5">
            <div className="row"> 
                <div className="col-md-6 offset-md-3">
                    <h4>Complete Registration</h4>
                   
                    {completeRegistrationForm()}
                </div>

            </div>
        </div>
    );
}
export default RegisterComplete;