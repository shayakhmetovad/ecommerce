import React, {useState, useEffect} from 'react';
import {auth} from '../../firebase';
import {toast} from 'react-toastify';
import {  useSelector } from 'react-redux';

const ForgotPassword = ({history}) => {
    const [email, setEmail] = useState('')
    const [loading, setLoading] = useState(false)
    const {user} = useSelector((state) => ({...state}))
    useEffect(()=> {
        if(user && user.token) {
            history.push('/')
        }
    }, [user, history])
    const handleSubmit = async(e) => {
        e.preventDefault();
        setLoading(true)
        const config = {
            url: process.env.REACT_APP_FORGOT_PASSWORD_REDIRECT_URL,
            handleCodeInApp: true
        }
        auth.sendPasswordResetEmail(email, config)
        .then((res)=> {
            setEmail('')
            setLoading(false)
            toast.success('Check your email for password reset link.');
        })
        .catch(err=> {
            setLoading(false)
            toast.error(err.message)
            console.log('Error msg in forgot password', err)
        })
    }
        
    return (
        <div className="container col-md-6 offset-md-3 p-5">
            {loading ? <h4 className="text-danger">Loading...</h4>:<h4>Forgot Password</h4>}
            <form onSubmit={handleSubmit}>
              <input placeholder="Your email" type="email" value={email} onChange={e=>setEmail(e.target.value)} className="form-control" autoFocus/> 
              <br /> 
              <button className="btn btn-raised" disabled={!email}>Submit</button>         
            </form>
        </div>
    );
}
export default ForgotPassword;