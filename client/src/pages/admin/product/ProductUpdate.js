import React, {useState, useEffect} from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import {toast} from 'react-toastify';
import {useSelector} from 'react-redux';
import { getCategories, getCategorySubs } from '../../../functions/category';
import FileUpload from '../../../components/forms/FileUpload';
import {LoadingOutlined} from '@ant-design/icons';
import { getProduct, updateProduct } from '../../../functions/product';
import ProductUpdateForm from '../../../components/forms/ProductUpdateForm';

const initialState = {
    title: '',
    description: '',
    price: '',
    // categories: [],
    category: '',
    subs: [],
    shipping: '',
    quantity: '',
    images: [],
    colors:  ["Black", "Brown", "Silver", "White", "Blue"],
    brands:  ["Apple", "Samsung", "Microsoft", "Lenovo", "Asus"],
    color: '',
    brand: ''
}

const ProductUpdate = ({match, history}) => {

    const [values, setValues] = useState(initialState)
    const [subOptions, setSubOptions] = useState([])
    const [categories, setCategories] = useState([])
    const [arrayOfSubs, setArrayOfSubIds] = useState([])
    const [selectedCategory, setSelectedCategory] = useState('')
    const [loading, setLoading] = useState(false)
    const {user} = useSelector(state => ({...state}))
    const {slug} = match.params;

    useEffect(()=> {
        loadProduct()
        loadCategories()
    }, [])
    const loadCategories = () => 
        getCategories().then(c=>setCategories(c.data));

    const loadProduct = () => {
        getProduct(slug)
        .then(product=> {
            // 1 load single product
            setValues({...values, ...product.data})
            // 2 load product's category subs
            getCategorySubs(product.data.category._id)
            .then(res=> {
                setSubOptions(res.data)
            })
            // 3 prepare array of sub ids to show as defalut sub values in antd select
            let arr = [];
            product.data.subs.map((s)=>{
                arr.push(s._id);
            })
            console.log(arr)
            setArrayOfSubIds((prev)=> arr) // required for antd select to work
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)
        values.subs = arrayOfSubs
        values.category = selectedCategory ? selectedCategory : values.category;

        updateProduct(slug, values, user.token)
        .then(res=> {
            setLoading(false)
            toast.success(`${res.data.title} is updated`)
            history.push('/admin/products');
        })
        .catch(err=>{
            console.log(err)
            setLoading(false)
            toast.error(err.response.data.err);
        })
    }
    const handleChange = (e) => {
        setValues({...values, [e.target.name]: e.target.value})
    }
    const handleCategoryChange = (e) => {
        e.preventDefault()
        setValues({...values, subs: []})
        setSelectedCategory(e.target.value)
        getCategorySubs(e.target.value).then(res=> {
            setSubOptions(res.data)
        })
        // if user clicks back to the original category
        // show is sub categories
        if(values.category._id === e.target.value) {
            loadProduct()
        }
        setArrayOfSubIds([])
    }
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2">
                    <AdminNav/>
                </div>
                <div className="col-md-10">
                    {loading ? (<LoadingOutlined className="text-danger h1"/>) : (<h4>Product update</h4>)}

                    <hr />
                    <div className="p-3">
                        <FileUpload 
                            values={values} 
                            setValues={setValues} 
                            setLoading={setLoading}
                        />
                    </div>
                    <br />
                    <ProductUpdateForm
                        subOptions={subOptions} 
                        arrayOfSubs={arrayOfSubs} 
                        setArrayOfSubIds={setArrayOfSubIds}
                        handleSubmit={handleSubmit} 
                        handleChange={handleChange} 
                        values={values} 
                        setValues={setValues}
                        handleCategoryChange={handleCategoryChange}
                        categories={categories}
                        selectedCategory={selectedCategory}
                    />
                </div>
            </div>
        </div>
    )
} 
export default ProductUpdate;