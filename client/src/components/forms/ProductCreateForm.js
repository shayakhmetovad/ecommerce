import React from 'react'
import { Select } from 'antd';

const { Option } = Select;

const ProductCreateForm = ({handleSubmit, handleChange, values, handleCategoryChange,subOptions, showSub, setValues}) => {
    //  desctructure
    const {title, description, price, category, quantity, subs, shipping, images, colors, brands, color, categories, brand} = values
    return (
        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label>Title</label>
                <input 
                    type="text" 
                    name="title" 
                    className="form-control" 
                    value={title} 
                    onChange={handleChange}
                />
            </div>
            <div className="form-group">
                <label>Description</label>
                <input 
                    type="text" 
                    name="description" 
                    className="form-control" 
                    value={description} 
                    onChange={handleChange}
                />
            </div>
            <div className="form-group">
                <label>Price</label>
                <input 
                    type="number" 
                    name="price" 
                    className="form-control" 
                    value={price} 
                    onChange={handleChange}
                />
            </div>
            <div className="form-group">
                <label>Shipping</label>
                <select className="form-control" onChange={handleChange} name="shipping">
                    <option>Please select</option>
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
            <div className="form-group">
                <label>Quantity</label>
                <input 
                    type="number" 
                    name="quantity" 
                    className="form-control" 
                    value={quantity} 
                    onChange={handleChange}
                />
            </div>
            <div className="form-group">
                <label>Color</label>
                <select className="form-control" onChange={handleChange} name="color">
                    <option>Please select</option>
                    {colors.map(c=> <option key={c} value={c}>{c}</option>)}
                </select>
            </div>
            <div className="form-group">
                <label>Brand</label>
                <select className="form-control" onChange={handleChange} name="brand">
                    <option>Please select</option>
                    {brands.map(b=> <option key={b} value={b}>{b}</option>)}
                </select>
            </div>
            <div className="form-group">
                <label>Category</label>
                <select name="category" className="form-control" onChange={handleCategoryChange}>
                    <option>Please select</option>
                    {categories.length>0 && 
                    categories.map((c)=> (
                        <option key={c._id} value={c._id}>{c.name}</option>
                    ))}
                    
                </select>
            </div>
                
            {showSub && (
            <div>
                <label>Sub Category</label>
                <Select
                    mode="multiple"
                    allowClear
                    style={{ width: '100%' }}
                    placeholder="Please select"
                    onChange={value => setValues({...values, subs: value})}
                    value={subs}
                >
                    {subOptions.length && subOptions.map(s=>(
                        <Option key={s._id} value={s._id}>{s.name}</Option>
                    ))}
                </Select>
            </div>
            )}
            <br />
            <button className="btn btn-outline-info mt-3">Save</button>
        </form>
    )
}
export default ProductCreateForm;