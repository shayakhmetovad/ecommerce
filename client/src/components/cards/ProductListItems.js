import React from 'react';
import { Link } from 'react-router-dom';

const ProductListItems = ({product}) => {
    const {price, category, subs, shipping, sold, quantity, color, brand} = product
    // console.log(product)
    return (
        <ul className="list-group list-group-flush">
            <li className="list-group-item d-flex justify-content-between">
              Price 
              <span className="label label-dafault label-pill pull-xs-right">$ {price}</span>
            </li>  
            {category && <li className="list-group-item d-flex justify-content-between">
              Category 
              <Link to={`/category/${category.slug}`} className="label label-dafault label-pill pull-xs-right">{category.name}</Link>
            </li>  }
            {subs && (
                <li className="list-group-item d-flex justify-content-between">
                    Sub categories
                    {subs.map((s)=>(
                        <Link to={`/sub/${s.slug}`} key={s._id} className="label label-dafault label-pill pull-xs-right">
                            {s.name}
                        </Link>
                    ))}
                </li>
            )}
            <li className="list-group-item d-flex justify-content-between">
              Shipping 
              <span className="label label-dafault label-pill pull-xs-right">{shipping}</span>
            </li>  
            <li className="list-group-item d-flex justify-content-between">
              Color 
              <span className="label label-dafault label-pill pull-xs-right">{color}</span>
            </li>  
            <li className="list-group-item d-flex justify-content-between">
              Brand 
              <span className="label label-dafault label-pill pull-xs-right">{brand}</span>
            </li>  
            <li className="list-group-item d-flex justify-content-between">
              Available 
              <span className="label label-dafault label-pill pull-xs-right">{quantity}</span>
            </li>  
            <li className="list-group-item d-flex justify-content-between">
              Sold 
              <span className="label label-dafault label-pill pull-xs-right">{sold}</span>
            </li>  
        </ul>
    )
}
export default ProductListItems;
