import firebase from "firebase/app";
import "firebase/auth";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCDQ7KS0tle4Soqpd4apXoZGNNpV605jTE",
    authDomain: "ecommerce-49649.firebaseapp.com",
    projectId: "ecommerce-49649",
    storageBucket: "ecommerce-49649.appspot.com",
    messagingSenderId: "397474756625",
    appId: "1:397474756625:web:c09a371e1071c3344c19b0"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

// export 
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();